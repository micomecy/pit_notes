问题:	properties载入中文出错

原因: 	properties文件编码不跟随工程编码设置,有单独的编码设置选项

解决:	在properties文件里面，编码格式使用系统默认格式不能直接点击修改  Editor - File Encodings/Default encoding for properties files: UTF - 8

![1535028449208](C:\Users\24676\Desktop\问题\image\1535028449208.png)